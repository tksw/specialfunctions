\documentclass[12pt, letterpaper]{article}

% Imported Packages
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{cleveref}
\usepackage{amssymb}


\setlength{\parindent}{0pt}
\graphicspath{{./images/}}

\title{Special Functions \\ \Large Chapter 2 - Partial Differential Equations}
\author{From the lectures of Dr. Mohammad Hassanzadeh \thanks{Typeset by SP}}
\date{Fall 2016}

\begin{document}
\begin{titlepage}
  \maketitle
  \newpage
  \tableofcontents
  \newpage
\end{titlepage}

\section{Introduction}
To get to this we need to use an important type of ODE. This is called an
Elementary Sturm-Liouville ODE. This is a special case of the so called
Sturm-Liouville problem in differential equations. The S\_L ODE is a second order
ODE which is linear. Let us consider the function $X(x)$ with ODE $= X''(x) =
\lambda X(x)$. \bigskip

\emph{Some Review}:
\begin{gather*}
  ay''+by'+cy = 0 \text{ can be changed to } am^2 + bm + c= 0 \\
  y = c_1 e^{m_1x} + c_2e^{m_2x}
\end{gather*}

In this problem, we are only interested in the non-zero solutions for $X(x)$
which is called the non-trivial solution. \bigskip

In this problem the values $\lambda$ which guarantee the non-zero solution
$X(t)$ is called the eigenvalue of the S\_L problem. The non-trivial solution
$X(x)$ is called the eigenfunction for the S\_L problem. \bigskip

However, in practice we solve the S\_L ODE $X''(x) = \lambda x$ with some
boundary (initial) conditions. \bigskip

Solving S\_L ODE with different boundary conditions. \bigskip

\emph{Remark}:


\begin{gather*}
  \underbrace{ X''=\lambda x, X(0) = 0, X(L) = 0 } \\
  \text{ (Sturm-Liouville with Dirichlet boundary condition) }\\
  \text{The Dirichlet S\_L ODE has the following non trivial solution:} \\
  \lambda = \lambda_n = -(\frac{n\pi}{L})^2, \: \: n = 1, 2, 3, \ldots \\
  X_n = \sin (\frac{n\pi x}{L}) \: \: n = 1, 2, 3
\end{gather*}

\newpage
\section{Solving Sturm-Liouville ODE with different conditions}

\begin{enumerate}[label=(\alph*)]
\item{S\_L with Dirichlet boundary condition}
  \begin{gather*}
    X'' = \lambda x \text{ where } X(0) = 0, X(L) = 0 \\
    \lambda = \lambda_n = -( \frac{n\pi}{L} )^2, X_n = \sin (\frac{n\pi x}{L}),
    n= 1,2,3, \ldots    
  \end{gather*}
\item{S\_L with Neumann boundary condition}
  \begin{gather*}
    X''=\lambda x \text{ where } X'(0) = X'(L) = 0\\
    \lambda = \lambda_n = -(\frac{n\pi}{L})^2, X_n = cos(\frac{n\pi x}{L}), n=0,1,2,3,\ldots
  \end{gather*}
\item{S\_L with Periodic Boundary Valve condition }
  \begin{gather*}
    X'' = \lambda x \text{ where }
    \begin{dcases}
      X(-L) = X(L) = 0 \\
      X'(-L) = X'(L) = 0 
    \end{dcases}
    \\
    \lambda = \lambda_n = -(\frac{n\pi}{L})^2, X_n = (\cos (\frac{n\pi x}{L}),
    \sin (\frac{n\pi x}{L})), n = 0,1,2,3, \ldots 
  \end{gather*}
\item{S\_L with Mixed Boundary Valve condition}
  \begin{gather*}
    X''=\lambda x \text{ where } X(0) = X'(L) = 0 \\
    \lambda = \lambda_n = \frac{(2n+1)\pi}{2L}, X_n = \sin (\frac{(2n+1)\pi
      x}{2L}), n = 0,1,2,3,\ldots
  \end{gather*}
\item{S\_L with Mixed Boundary Condition}
  \begin{gather*}
    X'' = \lambda x \text{ where } X'(0) = X(L) = 0 \\
    \lambda = \lambda_n = \frac{(2n+1)\pi}{2L}, X_n = \cos (\frac{(2n+1)\pi
      x}{2L}), n = 0,1,2,3,\ldots
  \end{gather*}
\end{enumerate}

\newpage

Proof of (a):
\begin{gather*}
  X'' = \lambda x, X(0) = X(L) = 0 \\ 
  X'' - \lambda x= 0 \to m^2 -\lambda  = 0 \text{ (Auxiliary Equation) } \\
  m^2 = \lambda \: \therefore \: m = \pm \sqrt{\lambda}
\end{gather*}

\newcommand{\desole}[1]{c_1 e^{\sqrt{\lambda} {#1}} + c_2 e^{-\sqrt{\lambda} {#1}}}
\newcommand{\desoltrig}[1]{c_1 \cos \sqrt{-\lambda} {#1} + c_2 \sin \sqrt{-\lambda} {#1}}

\begin{enumerate}[label=Case \arabic*:]
\item
  $\lambda > 0 \to$ 2 Solutions
  \begin{gather*}
    \text{General Solution: } X(x) = c_1e^{\sqrt{\lambda}x} + c_2e^{-\sqrt{\lambda}x} \\
    \text{Apply Boundary Conditions, } X(0)=0=c_2+c_2 \to c_1 = -c_2 \\
    X(L)=0=\desole{L} \to 0 = \desole{L} \to c_2 = 0 \therefore c_1 = 0 \\
    \therefore X(x) \equiv 0 \text{ \underline{trivial}}
  \end{gather*}
\item
  $\lambda = 0$
  \begin{gather*}
    X(x) = c_1 e^{0} + c_2xe^{0} = c_1 + xc_2 \\
    \text{Apply Boundary Conditions, } X(0) = 0 = c_1 = 0 \\
    X(L) =0 \to 0e^0 + c_2 L e^{-\sqrt{\lambda} L} \therefore c_2 = 0 \text{ \underline{trivial}}    
  \end{gather*}
\item
  $\lambda < 0$
  \begin{gather*}
    \text{Apply Boundary Conditions, } X(x) = \desoltrig{x} \\
    X(0) = 0 = 0 + c_1 \to c_1 = 0 \\
    X(L) = 0 = 0 + c_2 \sin \sqrt{-\lambda} L = 0 \therefore \sqrt{-\lambda}L = n \pi \\
    \text{Let us see that } n \neq 0 \therefore \lambda = 0, \\
    \therefore \sqrt{-\lambda}L = \pi n = 1, 2, 3, 4, \ldots \\
    \lambda = \lambda_n = -(\frac{\pi n}{L})^2 \text{ and } X(x) = c_2 \sin (\frac{\pi n}{L} x) = \sum_{n=1}^{\infty} a_n \sin(\frac{\pi n}{L}x)
  \end{gather*}
\end{enumerate}


Proof of (b):
\begin{gather*}
  X'' = \lambda x, X'(0) = X'(L) = 0 \\
  X''- \lambda x = 0 \to m^2 -\lambda = 0 \\
  m^2 = \lambda \therefore m = \pm \sqrt{\lambda}
\end{gather*}


\newcommand{\desoleTh}[3]{c_1 {#2} e^{\sqrt{\lambda} {#1}} + c_2 {#3} e^{-\sqrt{\lambda} {#1}}}
\begin{enumerate}[label=Case \arabic*: ]
\item{$\lambda > 0$}
  \begin{gather*}
    X(x) = \desole{x} \Rightarrow X'(x) = c_1 \sqrt{\lambda} e^{\sqrt{\lambda}x} - c_2 \sqrt{\lambda} e^{-\sqrt{\lambda} x} \\
    X'(0) = c_1 \sqrt{x} - c_2 \sqrt{\lambda} = 0 = \sqrt{\lambda}(c_1 - c_2) \Rightarrow c_1 - c_2 = 0, c_1 = c_2 \\
    X'(L) = c_1\sqrt{\lambda} e^{\sqrt{\lambda}L} - c_2 \sqrt{\lambda} e^{-\sqrt{\lambda}L} = 0 = c_1 \sqrt{\lambda} e^{\sqrt{\lambda}L} - c_1 \sqrt{\lambda} e^{-\sqrt{\lambda}L} = 0, \\ \rightarrow c_1 = 0 = c_2 \text{  \underline{trivial}}
  \end{gather*}
\item{$\lambda = 0$}
  \begin{gather*}
    X(0) = \desoleTh{0}{}{x} \Rightarrow X(0) = c_1 + c_2 x
  \end{gather*}
\end{enumerate}


\newpage
\input{/home/swp/Workspace/SpecialFunc/extras/useful.tex}

\end{document}

