\documentclass[12pt, a4paper]{article}

% Imported Packages

\usepackage[utf8]{inputenc} % Different Encodings UTF-8 because it's great
\usepackage{graphicx}       % Needed for pictures (for plots in this doc)
\usepackage{amsmath}        % Equations
\usepackage{mathtools}      % Equations w/ bug fixes (need to import ams before)
\usepackage{hyperref}       % Needed to import cleveref
\usepackage{cleveref}       % Automatically links to figures in PDF
\usepackage{enumitem}       % Needed ordered lists to start with alphabet 
\usepackage{amssymb}        % Symbols for equations (the therefore symbol)
\usepackage{array}          % Used for changing padding in tables
\usepackage{pbox}           % Allows use of \\ in cells in math mode in tables

% I don't like automatic indentation on every paragraph
\setlength{\parindent}{0pt}

% Keep a separate subdirectory for images (plot pictures)
\graphicspath{{./images/}}

\title{Special Functions \\ \Large Chapter 1 - Fourier Series}
\author{From the lectures of Dr. Mohammad Hassanzadeh \thanks{Typeset by SP}}
\date{Fall 2016}


\begin{document}

\begin{titlepage}
  \maketitle
  \newpage
  \tableofcontents
  \newpage
\end{titlepage}


\section{Introduction}

Definition - Piecewise Continuous Function: 
\begin{enumerate}
\item{A function $f$ on $[a, b]$ is called a piecewise continous function if $f$ is
    continous on all points of $[a, b]$ except perhaps finitely many $x$ like $x_1, \:
    x_2, \: x_3,\: \ldots \:  , \: x_k$}
\item{The left and right limits at $x_1, \: x_2, \: x_3, \: \ldots \:, \: x_k$ \underline{exist}.}
\end{enumerate}

\emph{Ex}: Every continous function is a piecewise function. \bigskip

\emph{Ex}: In this example $f(c)$ DNE, doesn't matter, limits still exist. 

\begin{figure}[ht]
  \centering
  \includegraphics[scale=0.75]{gph_ch1_1}
  \caption{A sample piecewise function  $\lim_{x \to b, c} f(x)$ exist}
\end{figure}

\emph{Ex}: $f(x) = \frac{1}{x}$ is not a piecewise continous function from $[-2,
2]$. \bigskip

Definition - Periodic Function: If $f(x) = f(x+a)$ for all x. \bigskip

Definition - Fourier Series:
\begin{enumerate}
\item{Let $f$ be a piecewise continous function on $[-\pi, \pi]$}
\item{The Fourier Series of $f$ is $f(x) = \frac{1}{2} a_0 + \sum_{n=1}^{\infty}
    a_n \cos(nx) + b_n \sin(nx)$}
\end{enumerate}

\emph{Remark}: Fourier Series of $f$ on $[-\pi, \pi]$ is
$ \frac{1}{2} a_0 + a_1\cos(x) + a_2\cos(2x) + a_3\cos(3x) + \ldots + a_1\sin(x) +
a_2\sin(2x) + a_3\sin(3x) + \ldots$ \bigskip

\emph{Note}: For now we can assume this infinite series is convergent. \bigskip

\emph{Remark}: The \# $a_0, a_1, a_2, \ldots, b_1, b_2, b_3, \ldots$ are called
Fourier coefficients.

\pagebreak[1]

\emph{Remark}: We can show that 

\begin{align}
  a_0 &= \frac{1}{\pi} \int_{-\pi}^{\pi} f(x) \:dx & \\
  a_n &= \frac{1}{\pi} \int_{-\pi}^{\pi} f(x) \cos(nx) \: dx & (n \geq 1) \\
  b_n &= \frac{1}{\pi} \int_{-\pi}^{\pi} f(x) \sin(nx) \: dx & (n \geq 1)           
\end{align}

Now let us find $a_0$

\begin{align*}
  \int_{-\pi}^{\pi} f(x) dx &= \int_{-\pi}^{\pi} \frac{1}{2} a_0 + \int_{-\pi}^{\pi} \sum_{n=1}^{\infty}  a_n \cos(nx) dx + \int_{-\pi}^{\pi} \sum_{n=1}^{\infty} b_n \sin(nx) dx \\
                            &= \pi a_0 + \sum_{n=1}^{\infty} \int_{-\pi}^{\pi} a_n \cos(nx) dx + \sum_{n=1}^{\infty} \int_{-\pi}^{\pi} b_n \sin(nx) dx \\
                            &= \pi a_0 \int_{-\pi}^{\pi} \frac{1}{2} a_0 + \sum_{n=1}^{\infty} a_n \int_{-\pi}^{\pi} \cos(nx) dx + \sum_{n=1}^{\infty} b_n \int_{-\pi}^{\pi} \sin(nx) dx \\
                            &= \pi a_0 + \sum_{n=1}^{\infty} (a_n \frac{1}{n} \sin(nx))|_{-\pi}^{\pi} + \sum_{n=1}^{\infty} (b_n \frac{-1}{n} \cos(nx))|_{-\pi}^{\pi} \\
                            &= \pi a_0 + 0 + 0 \\
  \int_{-\pi}^{\pi} f(x) dx    &= \pi a_0 \\
  \frac{1}{\pi} \int_{-\pi}^{\pi} f(x) dx &= a_0 \\
\end{align*}

\emph{Homework}: Find $a_n$ and $b_n$. (Hint: There are two cases for each.)
\bigskip

Remember the MacLaurin Series from previous courses? \bigskip

\emph{Ex}: $e^x = 1 + x + \frac{x^2}{2\!} + \frac{x^3}{3\!} \ldots = \sum_{n=0}^{\infty} \frac{f^n(a)}{n\!} (x-a)^n$ \bigskip

Fourier Series is similar in that regard except it only works for periodic
functions! \newpage
\emph{Ex}: Find the Fourier Series of $f(x) = \begin{dcases} -1, &  \pi < x < 0
  \\ 1,  &  0 \leq x < \pi  \end{dcases}$

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.75]{gph_ch1_2}
  \caption{The graph of $f(x)$, only within the given boundaries, remember it is periodic!}
\end{figure}

Finding $a_0$:
\begin{align*}
  a_0 &= \frac{1}{\pi} \int_{-\pi}^{\pi} f(x) dx \\
      &= \frac{1}{\pi} \int_{-\pi}^{0} -1 dx + \frac{1}{\pi} \int_{0}^{\pi} 1 dx \\
      &= \frac{1}{\pi} (-x) |_{-\pi}^{0} +  \frac{1}{\pi} (x) |_{0}^{\pi} \\
      &= -1+1 = 0
\end{align*}

Now to find $a_n$

\begin{align*}
  a_n &= \frac{1}{\pi} \int_{-\pi}^{\pi} f(x) \\cos(nx) \\
      &= \frac{1}{\pi}(\int_{-\pi}^{0} -\cos(nx) dx + \int_{0}^{\pi} \cos(nx) dx) \\
      &= \frac{1}{\pi} (\frac{-1}{n} \sin(nx) |_{-\pi}^0 + \frac{1}{n} \sin(nx) |_0^{\pi}) \\
      &= 0
\end{align*}

Therefore $a_1, a_2, a_3, \ldots = 0$ \bigskip

Now to find $b_n$

\begin{align*}
  b_n &= \frac{1}{\pi} \int_{-\pi}^{\pi} f(x) \sin(nx) \\
      &= \frac{1}{\pi} (\int_{-\pi}^{0} -\sin(nx) dx + \int_{0}^{\pi} \sin(nx) dx) \\
      &= \frac{1}{\pi} (\frac{1}{n} \cos(nx) |_{-\pi}^0 +\frac{1}{n}   \cos(nx) |_{0}^{\pi}) \\
      &=
        \begin{dcases}
          \frac{4}{n \pi} & n_{odd} \\ \\
          0 & n_{even}
        \end{dcases}   
\end{align*}

Therefore the final Fourier Series is:
\[ f(x) = \frac{4}{\pi} \sum_{n=odd}^{\infty}  \frac{1}{n} \sin(nx)  \]

Which can also be written as:
\[f(x) = \frac{2}{\pi} \sum_{n=0}^{\infty}  \frac{(1-(-1)^n)}{n} \sin(nx)  \]

Or:
\[f(x) = \frac{4}{\pi} \sum_{n=0}^{\infty}  \frac{1}{(2n+1)} \sin((2n+1)x)  \]

\emph{Homework}: Find the Fourier Series of the square wave $f(x)
= \begin{dcases} 0, &  -\pi \leq x < 0 \\ 1,  &  \: \: \: \: 0 \leq x < \pi  \end{dcases}$

\begin{figure}[h!]
  \includegraphics[scale=0.75]{gph_ch1_3}
  \caption{Graph of the given square wave}
  \label{fig:squarewave}
\end{figure}

\newpage
\clearpage

\section{Dirichlet's Theorem}

We define $s_n = \frac{1}{2} + \frac{2}{\pi} \sin(x) + \frac{2}{3 \pi} \sin(3x) +
\ldots + \frac{2}{n \pi} \sin(nx) \: \text{(where n is odd)}$ \bigskip

\emph{Ex}: $s_2 = \frac{1}{2} + \frac{2}{\pi} \sin(x) + \frac{2}{3 \pi} \sin(3x)$

\begin{figure}[ht]
  \centering
  \includegraphics[scale=0.75]{gph_ch1_4}
  \caption{Graphs of increasing partial sum functions}
\end{figure}

Definition - Dirichlet's Theorem:

If $f$ is a $2 \pi $ periodic function and $f$ and $f'$ are both piecewise
continous, then the Fourier series of $f$ is convergent. The sum of the Fourier
series of $f$ converges to the value of $f$ \underline{if} $f$ is continous at
that point. \bigskip

If $f$ is not continous at that point, then the Fourier series at $x$ is the
average of the left and right limits at that point: $$\frac{f(x^-) +
  f(x^+)}{2}$$

\newpage
\emph{Ex}:  $f(x) = \begin{dcases} 0, &  \pi \leq x < 0 \\ 1,  &  0 \leq x <
  \pi  \end{dcases}$ (Refer to \autoref{fig:squarewave} on Page
\pageref{fig:squarewave})

\begin{enumerate}[label=(\alph*)]
\item{Find the Fourier series of the function}
\item{Find the value of the Fourier series at $x=1$}
\item{Find the value of the Fourier series at $x=\frac{\pi}{2}$}
\item{Use part (c) to show that $\frac{\pi}{4} = 1- \frac{1}{3} + \frac{1}{5 }
    -\frac{1}{7} + \ldots$}
\item{Find the value of the Fourier series at $x=0$}
\end{enumerate}

Solutions:

\begin{enumerate}[label=(\alph*)]
\item{$f(x) \approx \frac{1}{2} + \frac{2}{\pi} \sin(x) + \frac{2}{3 \pi} \sin(3x) + \ldots$}
\item{$x=1, f$ is continous therefore the value of the Fourier series at $x=1$
    is $f(1) =1$}
\item{$x=\frac{\pi}{2}$, $f$ is continous there, therefore Fourier series at
    $(x=\frac{\pi}{2})$ = $f(\frac{\pi}{2}) = 1$}

\item{Use Part (c) to show: }
  \begin{align*}
    FS(\frac{\pi}{2}) = 1 &= \frac{1}{2} + \frac{2}{\pi} - \frac{2}{3\pi} + \ldots \\
    1 &=  \frac{1}{2} + \frac{2}{\pi} - \frac{2}{3\pi} + \ldots \\
    \frac{1}{2} &= \frac{2}{\pi} - \frac{2}{3\pi} + \frac{2}{5\pi} + \ldots \text{(multiply both sides by } \frac{\pi}{2}) \\
    \therefore \frac{\pi}{4} &= 1 - \frac{1}{3} + \frac{1}{5} - \frac{1}{7} + \ldots
  \end{align*}
\item{Since the function is not continous at 0, we therefore use the average of
    limits: $\frac{f(0^-) + f(0^+)}{2} = \frac{0+1}{2} = \frac{1}{2}$}
\end{enumerate}

\emph{Homework}: $f(x) = x \text{ where} -\pi < x < \pi$
\begin{enumerate}
\item{Compute the Fourier series of $f$.}
\item{Use part (a) to show $\frac{\pi}{4} = 1 - \frac{1}{3} + \frac{1}{5} -
    \frac{1}{7} + \ldots$}
\item{Find the value of the Fourier series at $x=3\pi$ and $x=5\pi$}
\end{enumerate}

\newpage
\section{Fourier Series of Odd/Even Functions}

Remember, odd functions are when $f(-x) = -f(x)$ and even functions are when
$f(-x) = f(x)$.

\begin{figure}[ht]
  \centering
  \includegraphics[scale=0.75]{gph_ch1_5}
  \caption{Graph of example odd (left) and even (right) functions.}
  \label{fig:oddeven}
\end{figure}

\emph{Remark}: $\int_{-a}^a odd = 0$ and $\int_{-a}^a even = 2 \int_{0}^a even $

{\renewcommand{\arraystretch}{1.8}%
  \begin{center}
    \begin{tabular}{| c | c | c |}
      \hline
      Fourier Coefficients & Odd $f(x)$ & Even $f(x)$ \\
      \hline
      $a_0$ & $\frac{1}{\pi} \int_{-\pi}^{\pi} f(x) dx = 0$ & $\frac{1}{\pi} \int_{-\pi}^{\pi} f(x) dx = 0$ \\
      \hline
      $a_n$ & $\frac{1}{\pi} \int_{-\pi}^{\pi} f(x) \cos(nx) dx = 0$ & \pbox{20cm}{\medskip$\frac{1}{\pi} \int_{-\pi}^{\pi} f(x) \cos nx dx$ \bigskip \\ $= \frac{2}{\pi} \int_{0}^{\pi} f(x) \cos nx dx$ \medskip} \\
      \hline
      $b_n$ & \pbox{20cm}{\medskip$\frac{1}{\pi} \int_{-\pi}^{\pi} f(x) \sin nx dx$ \bigskip \\ $= \frac{2}{\pi} \int_{0}^{\pi} f(x) \sin nx dx$ \medskip} & $\frac{1}{\pi} \int_{-\pi}^{\pi} f(x) \sin(nx) dx = 0$ \\
      \hline      
    \end{tabular}
  \end{center}}

\emph{Remark}: If we do $2\pi$ extension of a function then most possibly we get discontinuous points at $x = (2k+1)\pi$. \bigskip

Except when $f(-\pi) = f(\pi)$ like $f(x) = \lvert x \rvert$ on $[-\pi, \pi]$.

\begin{figure}[ht]
  \centering
  \includegraphics[scale=0.75]{gph_ch1_6}
  \caption{Graph of $\lvert x \rvert$, remember this only works since boundaries are closed!}
\end{figure}

\emph{Remark}: These are some functions which are only defined on $[0, \pi]$, rather than $[-\pi, \pi]$, these aren't $2\pi$ periodic.\bigskip

So can we write a Fourier Series for them? Yes, using two important methods, an odd extension or even extension, both from $[-\pi, \pi]$. \bigskip

For example, if we refer to the figure below, which is not defined from $[-\pi, 0]$, we can see that the graph on the left in \autoref{fig:oddeven} on Page \pageref{fig:oddeven} is an odd extension of it. Similarily, the function on the right side of the figure is an even extension of the same function.

\begin{figure}[ht]
  \centering
  \includegraphics[scale=0.75]{gph_ch1_7}
  \caption{Not $2\pi$ periodic!}
\end{figure}

\newpage


\emph{Homework}: Let $f(x)=x$ on $[0, \pi]$
\begin{enumerate}[label=(\alph*)]
\item{Find the odd/even extensions}
\item{Then find the Fourier series for both}
\end{enumerate}

\emph{Remark}: Since $f_{odd}(x)$ and $f_{even}(x)$ are the same on $[0, \pi]$ therefore these two Fourier series are equal (same value) on $[0, \pi]$.

\newpage

\section{Complex Fourier Series}

If $f$ is a piecewise continous function ($2 \pi$ periodic) the real Fourier
series is  $\frac{1}{2} a_0 + \sum_{n=1}^{\infty} a_n \cos(nx) + b_n \sin(nx)$.
\bigskip

We can use Euler's Formula ($e^{inx} = \cos(x) + i\sin(x)$) to change the
formula for real Fourier series to complex as follows:
\[
  \frac{a_0}{2} + \sum_{n=1}^{\infty} a_n \cos (nx) +  b_n  \sin (nx) =
  \sum_{n = -\infty}^{\infty} c_n e^{inx} 
\]

Proof:

\begin{align*}
  & \cos x  = \frac{e^{ix}+e^{-ix}}{2}, \: \sin x =  \frac{e^{ix}-e^{-ix}}{2i} \\
  & \therefore \:  \frac{a_0}{2} + \sum_{n=1}^{\infty} a_n \cos (nx) +  b_n  \sin (nx) \\
  &= \frac{a_0}{2} + \sum_{n=1}^{\infty} a_n  \frac{e^{inx}+e^{-inx}}{2} + b_n  \frac{e^{inx}-e^{-inx}}{2i} \\
  &= \frac{a_0}{2} + \sum_{n=1}^{\infty} (\frac{a_n}{2} +\frac{b_n}{2i}) e^{inx} + (\frac{a_n}{2} - \frac{b_n}{2i}) e^{-inx}  \\
  &= \frac{a_0}{2} + \sum_{n=1}^{\infty} (\frac{a_n - ib_n}{2}) e^{inx} + (\frac{a_n+ib_n}{2}) e^{-inx} \\
  &= \frac{a_0}{2} + \sum_{n=1}^{\infty} c_n e^{inx} + \sum_{n=1}^{\infty} c_{-n} e^{-inx} \\
  &= \sum_{n=-\infty}^{\infty} c_n e^{inx} 
\end{align*}

\begin{align*}
  c_0 &= \frac{1}{2 \pi} \int_{-\pi}^{\pi} f(x) dx \\
  c_n &= \frac{1}{2 \pi} \int_{-\pi}^{\pi} f(x) e^{-inx} dx \: \: \text{(Prove this!)} 
\end{align*} \bigskip

\textbf{My notes are a bit wonky here, missing some information.}

Find the Fourier series of $f(x) = \begin{dcases} -1, &  \pi < x < 0 \\ 1,  &
  0 < x < \pi  \end{dcases}$

\newcommand{\complexFourierInt}{\frac{1}{2 \pi} \int_{-\pi}^{\pi} f(x) e^{-inx} dx}
\newcommand{\realFourier} { \frac{a_0}{2} + \sum_{n=1}^{\infty} a_n \cos (nx) +
  b_n  \sin (nx)}
\newcommand{\complexFourier}{\sum_{n=-\infty}^{\infty} c_n e^{inx}}

\begin{align*}
  c_n &= \complexFourierInt \\
      &= \frac{1}{2\pi} \int_{-\pi}^{0} -e^{-inx} dx + \frac{1}{2\pi} \int_{0}^{\pi} e^{-inx} dx \\
      &= \frac{1}{2\pi in } e^{-inx} \rvert_{-\pi}^0 - \frac{1}{2\pi in } e^{-inx} \rvert_{0}^{\pi} \\
      &= (\frac{1}{2\pi in} - \frac{1}{2\pi in}e^{-inx}) + \frac{1}{2\pi in} (1- \cos(n\pi)) \\
      &= 0 + \frac{1}{2\pi in}(1-\cos(n\pi)) \\
      &= \frac{1}{\pi in} (1 - \cos(n\pi)) \\ \\
      &= \begin{dcases} \frac{2}{\pi n}, & n_{odd} \\ 0, & n_{even} \end{dcases}
\end{align*}



\section{Derivation \& Integration of Fourier Series}

\subsection{Derivation of Fourier Series}
\emph{Question}: Can we always do term by term derivation of Fourier series? \bigskip

\textbf{Short Answer}: No! \bigskip

\textbf{Long Answer}: If $f(x)$ is a $2\pi$ periodic function, continous and we
also have $f'(x)$ and $f''(x)$, both of which have to be piecewise continous,
then we can apply term by term derivation.  \bigskip

\begin{align*}
  f(x) &= \complexFourier = \realFourier \\
  f'(x) &= \sum_{n=-\infty}^{\infty} inc_ne^{inx} = \sum_{n=1}^{\infty} -na_n \sin (nx) + n b_n \cos (nx) \: \text{(Wherever $f'(x)$ exists)} 
\end{align*}

\begin{center}
  $\therefore \: \:$ Dirichlet's Theorem also applies.
\end{center}

\newpage
\emph{Remark}: Derivation of a Fourier series results in a Fourier series.
\bigskip

\emph{Ex}: Use Fourier series of $f(x) = x^2 = \frac{\pi^2}{3} + 4
\sum_{n=1}^{\infty} \frac{(-1)^n}{n^2} \cos (nx)$. \bigskip

Solution:
\begin{align*}
  f(x) = x^2 &= \frac{\pi^2}{3} + 4 \sum_{n=1}^{\infty} \frac{(-1)^n}{n^2} \cos (nx) \\
  2x &= 4 \sum_{n=1}^{\infty} \frac{(-1)^n}{n^2} (-1)(n) \sin (nx) \\
             &= 4 \sum_{n=1}^{\infty} \frac{(-1)^{n+1}}{n} \sin (nx) \\
  x &= 2 \sum_{n=1}^{\infty} \frac{(-1)^{n+1}}{n} \sin (nx)
\end{align*}

\subsection{Integration of Fourier Series}

\emph{Remark}: The Fourier series of a piecewise $2\pi$ periodic function $f(x)$
is $\realFourier$. Its indefinite integral is \underline{not} a periodic
function, nor is it a Fourier series. \bigskip

\emph{Remark}: In Fourier series:

$$ f(x) = \complexFourier = \realFourier$$

If $c_0 = a_0 = 0$ then we can do term by term integration. \bigskip

Let $F(x) = \int f(x)$. \bigskip

Then it follows that:

$$ F(x) = \sum_{n=-\infty}^{\infty} \frac{c_n}{in} e^{inx} =
\sum_{n=1}^{\infty} \frac{a_n}{n} \sin (nx) - \frac{b_n}{n} \cos (nx) + c$$

Now we find the constant $c$:

\newcommand{\intPiPi}{\int_{-\pi}^{\pi}}

\begin{align*}
  \intPiPi LHS &= \intPiPi RHS \\
  \intPiPi F(x) dx &= \sum_{n=1}^{\infty} \frac{a_n}{n} \intPiPi \sin (nx)\: dx - \frac{b_n}{n} \intPiPi \cos (nx) \: dx + \intPiPi c \: dx \\
  \intPiPi F(x) dx &= 0 + 0 + 2\pi c
\end{align*}

$$\therefore \: c = \frac{1}{2\pi} \intPiPi F(x) \: dx$$

If $c \neq 0$ and $a_0 \neq 0$ the formula would be:

$$ F(x) = a_0 x + \sum_{n=1}^{\infty} \frac{a_n}{n} \sin (nx) - \frac{b_n}{n}
\cos (nx) + \frac{1}{2\pi} \intPiPi F(x) dx$$

\emph{Ex}: Fourier series of $x = 2 \sum_{n=1}^{\infty} \frac{(-1)^{n+1}}{n}
\sin (nx)$  \bigskip

Solution:

\begin{align*}
  x &= 2 \sum_{n=1}^{\infty} \frac{(-1)^{n+1}}{n} \sin (nx) \\
  \frac{1}{2} x^2 &= 2 \sum_{n=1}^{\infty} \frac{(-1)^{n+1}}{n} \frac{-\cos (nx)}{n} + c \\
    &= 2 \sum_{n=1}^{\infty} \frac{(-1)^{n+2}}{n^2} \cos (nx) + c \\
  x^2 &= 4 \sum_{n=1}^{\infty} \frac{(-1)^{n+2}}{n^2} \cos (nx) + \frac{\pi^2}{3} 
\end{align*}

\emph{Question}: Do you think  $\sum_{n=1}^{\infty} \frac{n^2}{2} \sin (nx)$
could be a Fourier series of a function? \bigskip

\textbf{Answer}: No, because $\lim_{n \to \infty} b_n = \infty$, meaning it is
divergent.

\newpage
\section{Fourier Series on Other Intervals}

Fourier series of the piecewise continous function $f : [-L,L] \to \mathbb{R}$ is:

$$\frac{1}{2} a_0 + \sum_{n=1}^{\infty} a_n \cos (\frac{n\pi x}{L}) + a_n \sin
(\frac{n\pi x}{L})$$

where:
\begin{align*}
  a_0 &= \frac{1}{L} \int_{-L}^{L} f(x) dx \\
  a_n &= \frac{1}{L} \int_{-L}^{L} f(x) \cos (\frac{n\pi x}{L}) \: dx \\
  b_n &= \frac{1}{L} \int_{-L}^{L} f(x) \sin (\frac{n\pi x}{L }) \: dx \\
\end{align*} \bigskip

Find the Fourier series of $f(x) = \begin{dcases} 0, & -2 \leq x < 0 \\ 2-x, &
  \: \: \: \: 0 < x \leq 2 \end{dcases}$ on $[-2, 2]$

Let's find $a_0$:

\begin{align*}
  a_0 &= \frac{1}{2} \int_{-2}^{2} f(x) dx \\
      &= \frac{1}{2} \int_{-2}^{0} 0 dx + \frac{1}{2} \int_{0}^{2} (2-x) dx \\
      &= \frac{1}{2} [2x-\frac{x^2}{2}]_0^2 \\
      &= \frac{9}{2} (2) = 1 
\end{align*}

Now find $a_n$:

\begin{align*}
  a_n &= \frac{1}{2} \int_{-2}^2 f(x) \cos (\frac{n\pi x}{2}) dx \\
      &= \frac{1}{2} \int_{-2}^0 0 dx + \frac{1 }{2} \int_0^2 (2-x) \cos(\frac{n\pi x}{2}) dx \\
  \xRightarrow{\text{Integration by parts}} &=
                                              \begin{dcases}
                                                \frac{4}{n^2 \pi^2}, & n_{odd} \\
                                                0, & n_{even}
                                              \end{dcases}
\end{align*}

Similarily, $b_n = \frac{1}{2} \int_0^2 (2-x) \sin (\frac{n\pi x}{2}) dx =
\frac{2}{n\pi}$. \bigskip

Final Answer:
\begin{equation*}
  \frac{1}{2} + \sum_{n=odd}^{\infty} \frac{4}{n^2\pi^2} \cos (\frac{n\pi x}{2}) + \sum_{n=1}^{\infty} \frac{2}{n\pi} \sin (\frac{n\pi x}{2})
\end{equation*}

\emph{Remark}: Suppose the function is defined on $[0, L]$:
\begin{enumerate}[label=(\alph*)]
\item{We can use the odd extension of $f$ on $[-L, L]$ to find Fourier series
    a.k.a the Sine Fourier series:}
  \begin{equation*}
    \sum_{n=1}^{\infty} b_n \sin(\frac{n\pi x}{L}) \text{ where } b_n = \frac{2}{L} \int_0^L f(x) \sin (\frac{n\pi x}{L}) \: dx
  \end{equation*}
\item{We can use the even extension of $f$ on $[-L, L]$ to find Fourier
    series a.k.a the Cosine Fourier series:}
  \begin{equation*}
    \sum_{n=1}^{\infty} a_n \cos(\frac{n\pi x}{L}) \text{ where } a_n = \frac{2}{L} \int_0^L f(x) \cos (\frac{n\pi x}{L}) \: dx
  \end{equation*}
\end{enumerate}

\emph{Ex}: Let $f(x) = x$ on $0 \leq x \leq L$
\begin{enumerate}[label=(\alph*)]
\item{Find the Sine Fourier series}
\item{Find the Cosine Fourier series}
\end{enumerate}

Solution:

\begin{enumerate}[label=(\alph*)]
\item{Sine Fourier series}
  \begin{equation*}
    b_n = \frac{2}{L} \int_0^L x \sin (\frac{n\pi x}{L}) dx \xRightarrow{\text{Integration by parts}} \frac{(-1)^{n+1}2L}{n\pi}
  \end{equation*}
  Final Answer:
  \begin{equation*}
    \frac{2L}{\pi } \sum_{n=1}^{\infty} \frac{(-1)^{n+1}}{n} \sin (\frac{n\pi x}{L})
  \end{equation*}
\end{enumerate}

\newpage
\input{/home/swap/Workspace/old/special_functions/extras/useful.tex}
\end{document}
