from matplotlib import pyplot as plt
import numpy as np


def s_n(n, x):
    sum = 1 / 2.0
    for i in range(1, 2 * n, 2):
        sum += (2 / (i * np.pi)) * np.sin(i * x)
    return sum


x = np.linspace(-np.pi, np.pi)

s_1 = [s_n(1, xi) for xi in x]
s_2 = [s_n(2, xi) for xi in x]
s_7 = [s_n(7, xi) for xi in x]
s_15 = [s_n(15, xi) for xi in x]

fig, axs = plt.subplots(2, 2)

axs[0, 0].plot(x, s_1)
axs[0, 0].set_xlim([-np.pi, np.pi])
axs[0, 0].set_title("$s_1$")
axs[0, 0].grid(True)
axs[0, 0].set_xticks([-np.pi, 0, np.pi])
axs[0, 0].set_xticklabels(["$- \pi$", "0", "$\pi$"])

axs[0, 1].plot(x, s_2)
axs[0, 1].set_xlim([-np.pi, np.pi])
axs[0, 1].set_title("$s_2$")
axs[0, 1].grid(True)
axs[0, 1].set_xticks([-np.pi, 0, np.pi])
axs[0, 1].set_xticklabels(["$- \pi$", "0", "$\pi$"])

axs[1, 0].plot(x, s_7)
axs[1, 0].set_xlim([-np.pi, np.pi])
axs[1, 0].set_title("$s_7$")
axs[1, 0].grid(True)
axs[1, 0].set_xticks([-np.pi, 0, np.pi])
axs[1, 0].set_xticklabels(["$- \pi$", "0", "$\pi$"])

axs[1, 1].plot(x, s_15)
axs[1, 1].set_xlim([-np.pi, np.pi])
axs[1, 1].set_title("$s_{15}$")
axs[1, 1].grid(True)
axs[1, 1].set_xticks([-np.pi, 0, np.pi])
axs[1, 1].set_xticklabels(["$- \pi$", "0", "$\pi$"])

plt.savefig("../images/gph_ch1_4.png", bbox_inches="tight")
