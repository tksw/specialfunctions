from matplotlib import pyplot as plt
import numpy as np
import scipy.special as spl

x = np.linspace(2, 11, 150, endpoint=False)


def piecewise(x):
    print(x)
    if (x >= 2 and x < 5):
        return np.sqrt(x)
    elif (5 < x and x < 8):
        return 1.0
    elif (8 < x and x < 11):
        return 1 / x
    else:
        return None


y = list(map(piecewise, x))

fig, axs = plt.subplots(1, 1, sharex=False, sharey=False)

axs.plot(x, y, zorder=0)

circ = plt.Circle(
    (5, np.sqrt(5)),
    radius=0.08,
    facecolor="white",
    edgecolor="black",
    zorder=1)
axs.add_patch(circ)

circ = plt.Circle((5, 1), radius=0.08, facecolor="w", edgecolor="black")
axs.add_patch(circ)

circ = plt.Circle((8, 1), radius=0.08, facecolor="w", edgecolor="black")
axs.add_patch(circ)

circ = plt.Circle((8, 1 / 8.0), radius=0.08, facecolor="w", edgecolor="black")
axs.add_patch(circ)

dash_one_y = np.linspace(1, np.sqrt(5), 50)
dash_one_x = list(map(lambda x: 5, dash_one_y))

axs.plot(dash_one_x, dash_one_y, linestyle="dashed", color="black", zorder=0)

dash_two_y = np.linspace(1, 1 / 8.0, 50)
dash_two_x = list(map(lambda x: 8, dash_two_y))

axs.plot(dash_two_x, dash_two_y, linestyle="dashed", color="black", zorder=0)

axs.set_title("Piecewise Function")
axs.set_aspect("equal")
axs.set_xticks([2, 5, 8, 11])
axs.set_xticklabels(["a", "b", "c", "d"])
axs.set_xlim([0, 15])
axs.set_ylim([0, 4])
axs.grid(True)

plt.savefig("../images/gph_ch1_1.png", bbox_inches="tight")
