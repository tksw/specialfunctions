from matplotlib import pyplot as plt
from matplotlib import gridspec
import numpy as np

x = np.linspace(-np.pi, np.pi, 400)


def odd(x):
    return np.sin(x)


def even(x):
    return abs(np.sin(x))


odd_y = [odd(xi) for xi in x]
even_y = [even(xi) for xi in x]

gs = gridspec.GridSpec(1, 2, width_ratios=[1, 1])

axs0 = plt.subplot(gs[0])
axs0.plot(x, odd_y)
axs0.set_xlim([-np.pi, np.pi])
axs0.set_title("Odd Function $f(-x) = -f(x)$")
axs0.set_xticks([-np.pi, 0, np.pi])
axs0.set_xticklabels(["$-\pi$", 0, "$\pi$"])
axs0.grid(True)

axs1 = plt.subplot(gs[1])
axs1.plot(x, even_y)
axs1.set_xlim([-np.pi, np.pi])
axs1.set_title("Even Function $f(-x) = f(x)$")
axs1.set_xticks([-np.pi, 0, np.pi])
axs1.set_xticklabels(["$-\pi$", 0, "$\pi$"])
axs1.grid(True)

plt.savefig("../images/gph_ch1_5.png", bbox_inches="tight")
