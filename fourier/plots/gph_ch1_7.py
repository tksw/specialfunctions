from matplotlib import pyplot as plt
from matplotlib import gridspec
import numpy as np

x = np.linspace(-np.pi, np.pi, 400)


def odd(x):
    return np.sin(x)


def even(x):
    return abs(np.sin(x))


odd_y = [odd(xi) for xi in x]
even_y = [even(xi) for xi in x]

axs0 = plt.subplot()
axs0.plot(x, odd_y, color="blue")
axs0.set_xlim([0, np.pi])
axs0.set_title("Some function $f(x)$ defined in $[0, \pi]$")
axs0.grid(True)
axs0.set_xticks([0, np.pi])
axs0.set_xticklabels([0, "$\pi$"])

plt.savefig("../images/gph_ch1_7.png", bbox_inches="tight")
