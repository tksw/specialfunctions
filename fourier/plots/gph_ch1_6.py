from matplotlib import pyplot as plt
import numpy as np

pi = np.pi
tpi = 2 * np.pi
trpi = 3 * np.pi

x = np.linspace(-pi, pi, 250)

y = [abs(xi) for xi in x]
fig, axs = plt.subplots(1, 1)

axs.plot(x, y)
axs.plot(x + 2 * pi, y, color="blue")
axs.plot(x - 2 * pi, y, color="blue")

axs.set_xlim([-2 * pi, 2 * pi])
axs.set_xticks([-2 * pi, -pi, 0, pi, 2 * pi])
axs.set_xticklabels(["$-2\pi$", "$\pi$", "$0$", "$\pi$", "$2\pi$"])
axs.grid(True)
axs.set_title("Periodic $| x |$ in $[-\pi, \pi]$")

plt.savefig("../images/gph_ch1_6.png", bbox_inches="tight")
