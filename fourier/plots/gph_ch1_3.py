from matplotlib import pyplot as plt
import numpy as np
import scipy.special as spl


def neg_step(x):
    return 0


def pos_step(x):
    return 1


neg_x = np.linspace(-np.pi, 0, 50)
neg_y = list(map(neg_step, neg_x))

pos_x = np.linspace(0, np.pi, 50)
pos_y = list(map(pos_step, pos_x))

fig, axs = plt.subplots(1, 1)

axs.plot(neg_x, neg_y, zorder=0)
axs.plot(pos_x, pos_y, zorder=0, color="blue")

circ = plt.Circle((-np.pi, 0), 0.08, facecolor="black", edgecolor="black")
axs.add_patch(circ)

circ = plt.Circle((0, 0), 0.08, facecolor="white", edgecolor="black")
axs.add_patch(circ)

circ = plt.Circle((np.pi, 1), 0.08, facecolor="white", edgecolor="black")
axs.add_patch(circ)

circ = plt.Circle((0, 1), 0.08, facecolor="black", edgecolor="black")
axs.add_patch(circ)

axs.set_ylim([-2, 2])
axs.grid(True)

axs.set_aspect("equal")

axs.set_title("Square Wave")

plt.savefig("../images/gph_ch1_3.png", bbox_inches="tight")
